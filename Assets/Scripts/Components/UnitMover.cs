﻿using UnityEngine;
using System.Collections;

public class UnitMover : MonoBehaviour
{
    Vector3[] movePoints;
    public bool isMoving = false;
    float distanceTraveled = 0;
	float speed = 3f;

    public void StartMove(Vector3[] points)
    {
		movePoints = new Vector3[points.Length];
		points.CopyTo (movePoints, 0);
//        movePoints = points;
        isMoving = true;
        distanceTraveled = 0;
		Debug.Log ("movePoints " + movePoints[0] + " " + movePoints[1]);
        DistanceDrawing.Draw(points, 1000);
    }

    void Update()
    {

        if (isMoving)
        {
			distanceTraveled += Time.deltaTime * speed;
            transform.position = CutPath(movePoints, distanceTraveled);
        }
    }
    Vector3 CutPath(Vector3[] points, float distance)
    {
        float distanceAcc = 0;
        for (int i = 0; i < points.Length - 1; i++)
        {
            float pointsDistance = Vector3.Distance(points[i], points[i + 1]);
            distanceAcc += pointsDistance;
            if (distanceAcc > distance)
            {
                float restDistance = pointsDistance - (distanceAcc - distance);

				Vector3 resultPoint = (points[i + 1] - points[i]).normalized * restDistance + points[i];
				Debug.Log ("restDistance " + restDistance + ". resultPoint " + resultPoint);

				return resultPoint;
            }
        }
        isMoving = false;
		Debug.Log ("points " + points[0] + " " + points[1]);

        return points[points.Length - 1];
    }
}
