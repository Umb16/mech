﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleUnitComponent : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public NavMeshObstacle navMeshObstacle;
    public UnitMover unitMover;

    BattleUnit unitLogic;
    Vector3 lastPoint = Vector3.zero;
    float allDistance = 0f;

    public static BattleUnitComponent Init(BattleUnit unitLink)
    {
        GameObject newUnit = Instantiate<GameObject>(Resources.Load<GameObject>("MechPrefabs/SimpleMech"));
        BattleUnitComponent unitComponent = newUnit.GetComponent<BattleUnitComponent>();
        unitComponent.unitLogic = unitLink;
        unitComponent.unitLogic.OnUnitTurnStartEvent += unitComponent.SetNavMeshActive;
        newUnit.transform.position = unitComponent.unitLogic.position;
        return unitComponent;
    }


    public void SetNavMeshActive(bool value)
    {
        if (value)
        {
            navMeshObstacle.enabled = false;
            navMeshAgent.enabled = true;
        }
        else
        {
            navMeshAgent.enabled = false;
            navMeshObstacle.enabled = true;
        }
    }

	NavMeshPath targetPath = null;

    public void OnReceiveMovePoint(Vector3 point)
    {
        if (!IsMovingAvaliable()||Vector3.Distance(point, transform.position) < 0.5f)
            return;
        
		if(targetPath == null)
		{
			targetPath = new NavMeshPath();
		}

		if (Vector3.Distance(point, lastPoint) > 0.5f)
		{
			Debug.Log ("targetPath " + targetPath.corners.Length);
			bool findResult = navMeshAgent.CalculatePath(point, targetPath);
			Debug.Log ("targetPath2 " + targetPath.corners[0] + " " + targetPath.corners[1]);

	        if (findResult)
	        {
	            Vector3[] pathCorners = targetPath.corners;
	            allDistance = 0;
	            for (int i = 0; i < pathCorners.Length - 1; i++)
	            {
	                allDistance += Vector3.Distance(pathCorners[i], pathCorners[i + 1]);
	            }
	            DistanceDrawing.Draw(pathCorners, unitLogic.GetMaxPathLength());
	            lastPoint = point;
	        }
		}
		else
		{
			//   DistanceDrawing.Clear();
			//navMeshAgent.SetDestination(point);
//			Debug.Log ("targetPath.corners.Count " + targetPath.corners.Length);
			Debug.Log ("targetPath3 " + targetPath.corners[0] + " " + targetPath.corners[1]);
			if (allDistance <= unitLogic.GetMaxPathLength())
			{
				unitMover.StartMove(targetPath.corners );
				unitLogic.stats.bodyPart.ChangeEnergy(-unitLogic.GetPathEnergyCost(allDistance));
			}
			else
			{
				unitMover.StartMove(CutPath(targetPath.corners, unitLogic.GetMaxPathLength()));
				unitLogic.stats.bodyPart.ChangeEnergy(-unitLogic.GetPathEnergyCost(unitLogic.GetMaxPathLength()));
			}
			navMeshAgent.ResetPath();
		}
    }

    Vector3[] CutPath(Vector3[] points, float distance)
    {
        float distanceAcc = 0;
        List<Vector3> pointsList = new List<Vector3>();
        for (int i = 0; i < points.Length - 1; i++)
        {
            float pointsDistance = Vector3.Distance(points[i], points[i + 1]);
            distanceAcc += pointsDistance;
            if (distanceAcc > distance)
            {
                float restDistance = pointsDistance - (distanceAcc - distance);
                pointsList.Add(points[i]);
                pointsList.Add((points[i + 1] - points[i]).normalized * restDistance + points[i]);
                break;
            }
            else
            {
                pointsList.Add(points[i]);
            }
        }
        return pointsList.ToArray();
    }

    bool IsMovingAvaliable()
    {
        return !unitMover.isMoving;
    }

    void Update()
    {
      /*  if (navMeshAgent.hasPath)
        {
            if (allDistance - navMeshAgent.remainingDistance >= unitLogic.GetMaxPathLength())
            {
                navMeshAgent.ResetPath();
                navMeshAgent.velocity = Vector3.zero;
            }
        }*/
    }
}
