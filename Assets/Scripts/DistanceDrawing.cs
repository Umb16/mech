﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DistanceDrawing : MonoBehaviour
{

    /*public static DistanceDrawing Instance;

    void Awake()
    {
        Instance = this;
    }*/
    static List<GameObject> pointsOfCurrentPath = new List<GameObject>();

    public static void Draw(Vector3[] points, float reachableDistance)
    {
        Clear();
        float pointFrequency = 1;
        float pointCounter = pointFrequency;
        float distanceAcc = 0;
        Vector3[] newPoints = new Vector3[points.Length];
        points.CopyTo(newPoints, 0);
        GameObject point;
        
        for (int i = 0; i < newPoints.Length - 1; i++)
        {
            float distance = Vector3.Distance(newPoints[i], newPoints[i + 1]);
            if (pointCounter > distance)
            {
                pointCounter -= distance;
                distanceAcc += distance;
            }
            else
            {
                distanceAcc++;
               
                if (distanceAcc <= reachableDistance)
                {
                    point = Instantiate<GameObject>(Resources.Load<GameObject>("PathPrefabs/ReachablePoint"));
                }
                else
                {
              //      print(Vector3.Distance(newPointsOfCurrentPath[newPointsOfCurrentPath.Count - 1].transform.position, newPointsOfCurrentPath[0].transform.position));
                    point = Instantiate<GameObject>(Resources.Load<GameObject>("PathPrefabs/UnreachablePoint"));
                }
                newPoints[i] = Vector3.Lerp(newPoints[i], newPoints[i + 1], pointCounter / distance);
                point.transform.position = newPoints[i];
                pointCounter = pointFrequency;
                pointsOfCurrentPath.Add(point);
                i--;
            }

           /* point = Instantiate<GameObject>(Resources.Load<GameObject>("PathPrefabs/ReachablePoint"));
            point.transform.localScale = Vector3.one * 1.5f;
            point.transform.position = points[points.Length - 1];
            pointsOfCurrentPath.Add(point);*/
            // GameObject point =  Instantiate<GameObject>(Resources.Load<GameObject>("DistancePoint"));

        }

    }

    public static void Clear()
    {
        for (int i = 0; i < pointsOfCurrentPath.Count; i++)
        {
            Destroy(pointsOfCurrentPath[i]);
        }
        pointsOfCurrentPath.Clear();
    }

}
