﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour
{	
	public static Main instance;
	public Camera usedCamera;
	public LayerMask groundMask;

	public Canvas canvas;

	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		BattleManager.InitNewBattle();
	}

	void Update()
	{
		if(Input.GetMouseButtonUp(0))
		{
//			Debug.Log ("UIP");
			RaycastHit hit;

			Ray ray = usedCamera.ScreenPointToRay (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, -10f));
			Physics.Raycast (ray, out hit, 1000, groundMask);
			Debug.DrawRay (ray.origin, ray.direction * 100f, Color.yellow, 2f);
			if(hit.transform)
			{
				BattleManager.currentVizualizer.OnMouseClick(hit.point);
//				Debug.Log (hit.point.x + " " + hit.point.z);
			}
		}
	}
}
