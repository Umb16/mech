﻿using UnityEngine;
using System.Collections;

public class UnitStats
{
	public BaseBodyPart 		bodyPart;
	public BaseChassisPart 		chassisPart;
	public BaseWeaponPart[] 	weaponParts;

	public UnitStats()
	{
		bodyPart = new SimpleBodyPart ();
		chassisPart = new SimpleChassisPart ();
		weaponParts = new BaseWeaponPart[1]{ new SimpleWeaponPart() };

	}
}
