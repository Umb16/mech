﻿using UnityEngine;
using System.Collections;

public class SimpleWeaponPart : BaseWeaponPart
{
	public SimpleWeaponPart()
	{
		//Допустим пока значения заносятся из конструктора
		//В будующем их можно распаковывать из json, например
		maxHealth = 15;
		maxHealth = 15;
		baseDamage = 10;
		weight = 3;
	}
}
