﻿using UnityEngine;
using System.Collections;

public class SimpleBodyPart : BaseBodyPart
{
	public SimpleBodyPart()
	{
		//Допустим пока значения заносятся из конструктора
		//В будующем их можно распаковывать из json, например
		energyRecovey = 20;
		maxEnergyReserve = 30;
	}
}
