﻿using UnityEngine;
using System.Collections;

public class BaseBodyPart : BaseUnitPart
{
	public event OnTriggerEvent OnChangeEnergyEvent;

    /// <summary>
    /// Показывает сколько энергии будет восстанавливаться каждый ход.
    /// </summary>
    public float energyRecovey;

    /// <summary>
    /// Показывает количество энергии запасённое для текущего хода.
    /// </summary>
    public float energyReserve;

    /// <summary>
    /// Показывает максимально достижимое количество энергии, которое может быть запсено для хода.
    /// </summary>
    public float maxEnergyReserve;

    public void ChangeEnergy(float value)
    {
        energyReserve = Mathf.Max(0, energyReserve + value);
		OnChangeEnergyEvent();
    }
}
