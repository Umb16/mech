﻿using UnityEngine;
using System.Collections;

public class BaseUnitPart
{
	/// <summary>
	/// Максимальный запас здоровья детали
	/// </summary>
	public int maxHealth;

	/// <summary>
	/// Текущий запас здоровья детали
	/// </summary>
	public int health;

	/// <summary>
	/// Вес детали. Интересно в каких еденицах его отображать.
	/// </summary>
	public int weight;
}
