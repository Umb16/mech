﻿using UnityEngine;
using System.Collections;

public class BaseChassisPart : BaseUnitPart
{
	/// <summary>
	/// Скорость. Отражает пройденное расстояние за еденицу энергии.
	/// </summary>
	public float speed;
}
