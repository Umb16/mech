﻿using UnityEngine;
using System.Collections;

public class BattleUnit
{
    public UnitStats stats;
    public Vector3 position;

    public event OnTriggerBoolEvent OnUnitTurnStartEvent;

    public BattleUnit(Vector3 initPosition)
	{
		stats = new UnitStats ();
        position = initPosition;
	}

	public void OnUnitTurnStart()
	{
        OnUnitTurnStartEvent(true);
        stats.bodyPart.OnChangeEnergyEvent += BattleManager.battleUI.currentStatsComponent.UpdateInfo;
	}

	public void OnUnitTurnEnd()
	{
        OnUnitTurnStartEvent(false);
        stats.bodyPart.OnChangeEnergyEvent -= BattleManager.battleUI.currentStatsComponent.UpdateInfo;
	}

    public void RegeanEnergy()
    {
        stats.bodyPart.energyReserve = Mathf.Min(stats.bodyPart.maxEnergyReserve, stats.bodyPart.energyReserve+stats.bodyPart.energyRecovey);
    }

	public float GetMaxPathLength()
	{
		return stats.bodyPart.energyReserve * stats.chassisPart.speed;
	}

	public float GetPathEnergyCost(float distance)
	{
		return distance / stats.chassisPart.speed;
	}

}
