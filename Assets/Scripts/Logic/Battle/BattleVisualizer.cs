﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleVisualizer
{
	Battle sourceBattle;
	List<BattleUnitComponent> spawnedUnits = new List<BattleUnitComponent> ();

	public static BattleVisualizer InitBattle(Battle sourceBattle)
	{
		BattleVisualizer newVizualizer = new BattleVisualizer ();
		newVizualizer.sourceBattle = sourceBattle;

		return newVizualizer;
	}

	public BattleVisualizer()
	{
	}

	public void PlaceUnits()
	{
		for(int i = 0; i < sourceBattle.unitsInBattle.Count; i++)
		{
			spawnedUnits.Add(BattleUnitComponent.Init(sourceBattle.unitsInBattle[i]));
		}
	}

	public void OnMouseClick(Vector3 point)
	{
		spawnedUnits[sourceBattle.currentUnitIndex].OnReceiveMovePoint(point);
	}
}
