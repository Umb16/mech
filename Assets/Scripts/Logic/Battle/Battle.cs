﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void OnTriggerEvent();
public delegate void OnTriggerBoolEvent(bool value);

public class Battle
{
	public int roundNumber;
	public List<BattleUnit> unitsInBattle;
	public int currentUnitIndex = 0;
	public BattleUnit CurrentUnit
	{
		get
		{
			return unitsInBattle[currentUnitIndex];
		}
	}

	public event OnTriggerEvent OnRoundStart;

	public Battle()
	{
		unitsInBattle = new List<BattleUnit> ();

		BattleUnit someUnit = new BattleUnit (Vector3.zero);
		unitsInBattle.Add (someUnit);

		someUnit = new BattleUnit (new Vector3(10,0,3));
		unitsInBattle.Add (someUnit);

        OnRoundStart += OnRoundStarted;
    }

    void OnRoundStarted()
    {
		currentUnitIndex = 0;
        for (int i = 0; i < unitsInBattle.Count; i++)
        {
            unitsInBattle[i].RegeanEnergy();
        }
		GiveTurnToNextUnit(true);
    }

	public void StartBattle()
	{
		roundNumber = 0;
		
		OnRoundStart();
	}

	public void GiveTurnToNextUnit(bool isFirstTurn = false)
	{
		if(!isFirstTurn)
		{
			currentUnitIndex++;
		}

		if (currentUnitIndex >= unitsInBattle.Count)
		{
			OnRoundStart();

		}
		else
		{
			CurrentUnit.OnUnitTurnStart();
			BattleManager.battleUI.currentStatsComponent.UpdateInfo ();
		}
	}

	public void OnUnitTurnComplete()
	{
		CurrentUnit.OnUnitTurnEnd();
		GiveTurnToNextUnit ();
	}

}
