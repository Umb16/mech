﻿using UnityEngine;
using System.Collections;

public class BattleManager
{
	public static Battle 			currentBattle;
	public static BattleVisualizer	currentVizualizer;
	public static BattleUI			battleUI;

	public static void InitNewBattle()
	{
		currentBattle = new Battle ();

		currentVizualizer = BattleVisualizer.InitBattle (currentBattle);
		currentVizualizer.PlaceUnits();

		GameObject battleUIObject = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("UIPrefabs/BattleUI"));
		battleUIObject.transform.SetParent(Main.instance.canvas.transform, false);

		battleUI = battleUIObject.GetComponent<BattleUI> ();

		currentBattle.OnRoundStart += battleUI.OnRoundStart;
//		currentBattle.OnUnitTurnStart += battleUI.OnUnitTurnStart;
//		currentBattle.OnUnitTurnStart += battleUI.currentStatsComponent.UpdateInfo;

		currentBattle.StartBattle();
	}
}
