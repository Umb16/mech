﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BattleUI : MonoBehaviour
{
	public CurrentUnitStats currentStatsComponent;
	public Text roundLabel;

	public void OnRoundStart()
	{
		roundLabel.text = "Раунд " + (BattleManager.currentBattle.roundNumber + 1).ToString();
	}

	public void OnUnitTurnStart()
	{
	}

	public void OnNextRoundClick()
	{
		BattleManager.currentBattle.OnUnitTurnComplete ();
	}
}
