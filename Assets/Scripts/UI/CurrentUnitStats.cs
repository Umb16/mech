﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CurrentUnitStats : MonoBehaviour
{
	public Text currentEnergy;
	public Text currentHealth;

	public void UpdateInfo()
	{
		currentEnergy.text = "Энергия: " + BattleManager.currentBattle.CurrentUnit.stats.bodyPart.energyReserve.ToString("0.#") + "/" + BattleManager.currentBattle.CurrentUnit.stats.bodyPart.maxEnergyReserve;
		currentHealth.text = "Тело: " + BattleManager.currentBattle.CurrentUnit.stats.bodyPart.health.ToString("0.#") + "/" + BattleManager.currentBattle.CurrentUnit.stats.bodyPart.maxHealth + "\n" + 
			"Шасси: " + BattleManager.currentBattle.CurrentUnit.stats.chassisPart.health.ToString("0.#") + "/" + BattleManager.currentBattle.CurrentUnit.stats.chassisPart.maxHealth;
	}
}
