﻿using UnityEngine;
using System.Collections;

public class CameraMover : MonoBehaviour {

    Transform target;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (target != null)
        {
            transform.position += transform.position *Mathf.Min(1,(Time.deltaTime)) + target.position * Mathf.Max(0, (1-Time.deltaTime));
        }
	
	}
}
